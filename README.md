# Mexico Coworking 2020

## Where?

At the beautiful beaches of [Sayulita, Jalisco](https://www.puertovallarta.net/what_to_do/sayulita-nayarit)

## When?

TBD - https://gitlab.com/jivanvl/gitlab-mexico-coworking-2020/-/issues/4



## FAQ

### Which airports are nearby?

* Guadalajara International Airport - GDL
* Puerto Vallarta - PVR

### Planning on attending?

If you wish to attend and are unsure on who might be attending.
We have a list of confirmed and to be confirmed attendees [here](https://gitlab.com/jivanvl/gitlab-mexico-coworking-2020/issues/1)

### Is the food awesome?

[Oh yes!](http://www.donpedros.com/menus-2/)
